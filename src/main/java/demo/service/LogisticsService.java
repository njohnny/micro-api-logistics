package demo.service;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.BooleanUtils;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import demo.entities.LogisticsEntity;
import demo.kafka.LogisticsConsumer;
import demo.kafka.LogisticsProducer;
import demo.model.LogisticsOrderDTO;
import demo.model.PaymentOrderDTO;
import demo.repository.order.LogisticsRepository;



@Service
public class LogisticsService {


	@Autowired
	private LogisticsRepository repository;
	
	@Autowired
	private LogisticsProducer logisticProducer;
	

	public List<LogisticsEntity> getPayments() {

		return this.repository.findAll();			
	}


	public LogisticsEntity getPayment(Long paymentId) {

		return this.repository.findById(paymentId).get();			
	}


	public LogisticsEntity create(LogisticsEntity Payment) {

		return this.repository.save(Payment);			
	}


	public LogisticsEntity update(LogisticsEntity payment) {

		return this.repository.save(payment);			
	}


	public boolean delete(Long id) {

		this.repository.deleteById(id);

		return true;			
	}

	

	public LogisticsEntity scheduleDelivery(PaymentOrderDTO orderPaymentDTO) {
	
		LocalDateTime deliveryDate  = LocalDateTime.now();
		Long orderId = orderPaymentDTO.getOrderId();
		
		LogisticsEntity logistics = new LogisticsEntity();
		LogisticsOrderDTO logisticsDTO = new LogisticsOrderDTO();
		
		
		
		if(orderId != null && BooleanUtils.isTrue(orderPaymentDTO.getAproved())) {
			logistics.setOrderId(orderId);
			LocalDateTime finalDeliveryDate = deliveryDate.plusDays(4);
			logistics.setDeliveryDateTime(finalDeliveryDate);
			this.create(logistics);
		System.out.println("ORDER LOGISTICS !! - ORDER LOGISTICS CREATED - DELIVERY DATE CREATED!");
		
		}
		
		logisticsDTO.setOrderId(logistics.getOrderId());
		logisticProducer.send("delivery-SCHEDULED", logisticsDTO);
		
		return logistics;
	}


	

		
	



}

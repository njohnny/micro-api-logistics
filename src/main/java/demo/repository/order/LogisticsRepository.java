package demo.repository.order;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import demo.entities.LogisticsEntity;

@Repository
public interface LogisticsRepository extends JpaRepository<LogisticsEntity, Long> {

	
	
}
package demo.kafka;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Component;

import demo.ApplicationLogistics;
import demo.model.LogisticsOrderDTO;

@Component
public class LogisticsProducer  {

	private static final Logger LOG = LoggerFactory.getLogger(ApplicationLogistics.class);


	@Autowired
	private KafkaTemplate<String,Object> templateProducer;

	public void send(String topic,LogisticsOrderDTO payment) {

		templateProducer.send(topic, payment);

		LOG.info("LOGISTICS: Sent method Finalized [{}] to topic [{}]", payment, topic);
	}



}

package demo.kafka;

import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;

import demo.ApplicationLogistics;
import demo.model.LogisticsOrderDTO;
import demo.model.PaymentOrderDTO;
import demo.service.LogisticsService;

@Component
public class LogisticsConsumer {

	@Autowired
	private LogisticsService logisticsService;
	
		private static final Logger LOG = LoggerFactory.getLogger(ApplicationLogistics.class);


		  @KafkaListener(topics = "${app.kafka.consumer.topics}", autoStartup = "${app.kafka.consumer.enabled}",
				  properties= {"spring.json.value.default.type=demo.model.PaymentOrderDTO"})
		  public void onMessage(ConsumerRecord<String, PaymentOrderDTO> record) {
			  
			  PaymentOrderDTO orderPaymentDTO = new PaymentOrderDTO();
			  
				orderPaymentDTO = record.value();
		    
			  LOG.info("LOGISTICS: CONSUMER-[payment-APPROVED] - Received record [{}], topic: {}, partition: {}, offset: {}",  record.value(), record.topic(), record.partition(), record.offset());
			logisticsService.scheduleDelivery(orderPaymentDTO);
			
			LOG.info("Passou no agendamento de entrega !");
		  }
	

}

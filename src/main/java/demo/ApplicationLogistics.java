package demo;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.kafka.annotation.EnableKafka;


@SpringBootApplication
@EnableEurekaClient
@EnableAutoConfiguration 
@EnableKafka
public class ApplicationLogistics {

    public static void main(String[] args) {
    	
    	new SpringApplication(ApplicationLogistics.class).run(args);
    	    	
    	System.out.println("<<< Demo Microservices: Logistics Service started >>>");
    }
       
	
}

